#ifndef PROCESS_CPU
#define PROCESS_CPU

#include "storage.h"

namespace processing {
   extern int n_device;
}

void prepare_processing();

size_t setup(const size_t);

void process(const size_t, storage_t&, storage_t&);

void cleanup(const size_t);

#endif
