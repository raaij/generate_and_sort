#ifndef GENERATE
#define GENERATE

#include <vector>
#include "storage.h"

struct Ranvec1;

std::tuple<storage_t, storage_t> generate(Ranvec1& random);

void generator(const size_t gid, queue_t& queue, int seed0, const int seed1);

#endif
