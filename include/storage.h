#ifndef STORAGE
#define STORAGE

#ifdef HAVE_CUDA
#include <thrust/host_vector.h>
#else
#include <vector>
#endif

#include "aligned_allocator.h"

namespace storage {
   const int n_dom = 115, n_mod = 18;
   extern int n_per_mod;
   extern int n_hits;
}

#ifdef HAVE_CUDA
using storage_t = thrust::host_vector<int, aligned_allocator<int, 32>>;
#else
using storage_t = std::vector<int>;
#endif

using queue_t = std::vector<std::tuple<storage_t, storage_t>>;

#endif
