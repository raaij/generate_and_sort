#include <map>
#include <iostream>
#include <tuple>

#include "storage.h"
#include "process.h"

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include <thrust/tuple.h>
#include <thrust/pair.h>
#include <thrust/system/cuda/execution_policy.h>

namespace {
   std::map<size_t, cudaStream_t*> streams;
   std::map<size_t, thrust::pair<thrust::device_vector<int>*,
                                 thrust::device_vector<int>*>> store;
   using std::cout;
   using std::endl;
}

int processing::n_device = 0;

void prepare_processing() {
   cudaGetDeviceCount(&processing::n_device);
}

size_t setup(const size_t id) {

   size_t device = id % processing::n_device;
   cudaSetDevice(device);

   auto it = streams.emplace(id, new cudaStream_t{});
   auto* stream = it.first->second;

   auto store_it = store.emplace(id, thrust::make_pair(new thrust::device_vector<int>,
                                                       new thrust::device_vector<int>));
   auto* times = store_it.first->second.first;
   auto* values = store_it.first->second.second;

   times->resize(storage::n_hits);
   values->resize(storage::n_hits);

   cudaStreamCreate(stream);
   return device;
}

void process(const size_t id, storage_t& times, storage_t& values) {

   auto& stream = *(streams[id]);

   auto& store_entry = store[id];
   auto& device_times = *(store_entry.first);
   auto& device_values = *(store_entry.second);

   // copy to device
   thrust::copy(thrust::cuda::par.on(stream),
                times.begin(), times.end(),
                device_times.begin());
   thrust::copy(thrust::cuda::par.on(stream),
                values.begin(), values.end(),
                device_values.begin());

   // sort
   thrust::sort_by_key(thrust::cuda::par.on(stream),
                       device_times.begin(), device_times.end(),
                       device_values.begin());

   // copy to host
   thrust::copy(thrust::cuda::par.on(stream),
                device_times.begin(), device_times.end(),
                times.begin());
   thrust::copy(thrust::cuda::par.on(stream),
                device_values.begin(), device_values.end(),
                values.begin());

   // synchronize
   cudaStreamSynchronize(stream);
}

void cleanup(const size_t id) {
   auto* stream = streams[id];
   cudaStreamDestroy(*stream);
   delete stream;

   auto& store_entry = store[id];
   delete store_entry.first;
   delete store_entry.second;
}
