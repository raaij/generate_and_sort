from ROOT import TFile
from ROOT import RooFit
from ROOT import RooRealVar, RooCBShape
from ROOT import RooArgList, RooDataHist

f = TFile("data/histos.root", "read")
h_tot = f.Get("histo_tot")

tot = RooRealVar("tot", "tot", 0.5, 40.5)
alpha = RooRealVar("alpha", "alpha", 1., 0, 5)
n = RooRealVar("n", "n", 2., 0, 20)
mean = RooRealVar("mean", "mean", 27, 20, 30)
sigma = RooRealVar("sigma", "sigma", 2.5, 1., 5.)
cb = RooCBShape("cb", "cb", tot, mean, sigma, alpha, n)

dh = RooDataHist("dh", "dh", RooArgList(tot), RooFit.Import(h_tot))

cb.fitTo(dh, RooFit.Minimizer("Minuit2"), RooFit.Offset(True))

frame = tot.frame()
dh.plotOn(frame)
cb.plotOn(frame)

frame.Draw()
