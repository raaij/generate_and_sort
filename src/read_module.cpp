#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <vector>


#include <TH1.h>
#include <TFile.h>
#include <TTree.h>
#include <TCanvas.h>
#include <TApplication.h>
#include <TROOT.h>
#include <TRootCanvas.h>
#include <ROOT/TTreeProcessorMT.hxx>
#include <ROOT/TThreadedObject.hxx>


namespace {
   using std::vector;
   using std::unordered_map;
   using std::endl;
   using std::cout;
   using std::string;
}

int main(int argc, char* argv[]) {

   string input_file = "data/differences.root";
   if (argc > 1) {
      input_file = argv[1];
   }
   TApplication app{"app", &argc, argv};

   app.SetReturnFromRun(true);

   // First enable implicit multi-threading globally, so that the implicit parallelisation is on.
   // The parameter of the call specifies the number of threads to use.
   int nthreads = 4;
   ROOT::EnableImplicitMT(nthreads);

   // Create one TThreadedObject per histogram to fill during the processing of the tree
   ROOT::TThreadedObject<TH1D> histo_diff{"histo_diff", "histo_diff", 3050, -500, 30000};
   ROOT::TThreadedObject<TH1I> histo_tot{"histo_tot", "histo_tot", 63, 0.5, 64.5};
   ROOT::TThreadedObject<TH1I> histo_pmt{"histo_pmt", "histo_pmt", 31, -0.5, 30.5};

   // Create a TTreeProcessorMT: specify the file and the tree in it
   ROOT::TTreeProcessorMT processor{input_file.c_str(), "diff_tree"};

   // Define the function that will process a subrange of the tree.
   // The function must receive only one parameter, a TTreeReader,
   // and it must be thread safe. To enforce the latter requirement,
   // TThreadedObject histograms will be used.
   auto process = [&histo_diff, &histo_tot, &histo_pmt](TTreeReader &reader) {
      TTreeReaderValue<int> diff{reader, "diff"};
      TTreeReaderValue<int> val{reader, "val"};

      // For performance reasons, a copy of the pointer associated to this thread on the
      // stack is used
      auto hd = histo_diff.Get();
      auto htot = histo_tot.Get();
      auto hpmt = histo_pmt.Get();

      while (reader.Next()) {
         hd->Fill(*diff);
         int v = *val;
         int tot = v & 0xff;
         int pmt = (v >> 8) & 0x1f;
         // int module = v >> 13;
         hpmt->Fill(pmt);
         htot->Fill(tot);
      }
   };

   // unordered_map<int, int> modules;
   // for (size_t i = 0; i < tree->GetEntries(); ++i) {
   //    tree->GetEntry(i);
   //    auto module = val >> 13;
   //    modules[module] += 1;
   // }

   // vector<int> mods;
   // mods.reserve(modules.size());

   // double av = 0, n = 0;
   // for (const auto& entry : modules) {
   //    mods.push_back(entry.first);
   //    av += entry.second; ++n;
   //    for (int i = 0; i < entry.second; ++i) {
   //       histo.Fill(entry.first);
   //    }
   // }
   // av /= n;

   // std::sort(begin(mods), end(mods));
   // for (const auto mod : mods) {
   //    cout << mod << " " << modules[mod] << endl;
   // }
   // cout << endl;

   // Launch the parallel processing of the tree
   processor.Process(process);

   // Use the TThreadedObject::Merge method to merge the thread private histograms
   // into the final result
   auto hdiff = histo_diff.Merge();
   auto htot = histo_tot.Merge();
   auto hpmt = histo_pmt.Merge();

   auto canvas = new TCanvas("canvas", "canvas", 450, 900);
   canvas->Divide(1, 3);
   auto rc = dynamic_cast<TRootCanvas*>(canvas->GetCanvasImp());

   // Make sure we exit when the TBrowser window is closed.
   app.Connect(rc, "CloseWindow()",
               "TApplication", &app, "Terminate()");

   canvas->cd(1);
   hdiff->Draw();
   canvas->cd(2);
   hpmt->SetMinimum(0);
   hpmt->Draw();
   canvas->cd(3);
   htot->Draw();

   TFile file{"histos.root", "recreate"};
   file.WriteTObject(htot.get());
   file.WriteTObject(hpmt.get());
   file.WriteTObject(hdiff.get());
   file.Close();

   app.Run();

   return 0;


}
