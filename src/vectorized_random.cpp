#undef __CUDACC_VER__
#define __CUDACC_VER__ 901000

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <tuple>
#include <thread>
#include <bitset>
#include <functional>

// #include <signal.h>

// #include <TFile.h>
// #include <TTree.h>

#include <zmq.hpp>
#include <ZeroMQSvc.h>

#include "stacktrace.h"
#include "storage.h"

#include "generate.h"

#include "process.h"

#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>

namespace {

   using std::cout;
   using std::cerr;
   using std::endl;
   using std::vector;
   using std::string;
   using std::to_string;
   using std::setw;
   using std::make_tuple;

   const size_t n_generators = 10;
   const size_t n_processors = 20;

   const int seed0 = 246233421, seed1 = 34136856;
}

// void segv_handler(int sig) {
//    raise (SIGABRT);
// }

void timer(const size_t id) {

   zmq::socket_t control = zmqSvc().socket(zmq::PAIR);
   zmq::setsockopt(control, zmq::LINGER, 0);

   auto con = connection(id);
   try {
      control.connect(con.c_str());
   } catch (const zmq::error_t& e) {
      cout << "failed to connect connection " << con << endl;
      throw e;
   }

   zmq::pollitem_t items [] = {
      {control, 0, ZMQ_POLLIN, 0},
   };

   bool started = 0;
   while (true) {

      // Wait 100 ms and then send signal
      zmq::poll(&items[0], 1, 100);

      if (items[0].revents & ZMQ_POLLIN) {
         auto done = zmqSvc().receive<bool>(control);
         if ((started && done) || (!started && !done)) {
            break;
         } else if (!started && done) {
            started = true;
         }
      } else if (started) {
         // Indicate to the main thread that a slice is ready
         zmqSvc().send(control, true);
      }
   }
}

void processor(const size_t pid, queue_t& queue) {

   zmq::socket_t control = zmqSvc().socket(zmq::PAIR);
   zmq::setsockopt(control, zmq::LINGER, 0);

   auto con = connection(pid);
   try {
      control.connect(con.c_str());
   } catch (const zmq::error_t& e) {
      cout << "failed to connect connection " << con << endl;
      throw e;
   }

   zmq::pollitem_t items [] = {
      {control, 0, ZMQ_POLLIN, 0},
   };

   boost::optional<storage_t> times, values;

   auto d = setup(pid);

   // Indicate to the main thread that we are ready to process
   boost::optional<bool> good;
   do {
      try {
         good = zmqSvc().send(control, d);
      } catch (const zmq::error_t& err) {
         if (err.num() == EINTR) continue;
      }
   } while (!good);

   while (true) {

      // Wait until we need to process
      boost::optional<int> n;
      do {
         try {
            n = zmq::poll(&items[0], 1, -1);
         } catch (const zmq::error_t& err) {
            cout << "processor caught exception." << err.what() << endl;
            if (err.num() == EINTR) continue;
         }
      } while (!n);

      n.reset();


      boost::optional<int> idx;
      if (items[0].revents & ZMQ_POLLIN) {
         idx = zmqSvc().receive<int>(control);
         if (*idx == -1) break;
      }

      if (idx) {
         // process a slice
         std::tie(times, values) = queue[*idx];
         process(pid, *times, *values);

         // signal that we're done
         zmqSvc().send(control, true);
      }
   }

   cleanup(pid);

}

int main(int argc, char* argv[]) {
   // signal(SIGSEGV, segv_handler);

   int max_events = 100;
   if (argc > 1) {
      try {
         max_events = boost::lexical_cast<int>(argv[1]);
         if (max_events < 1) {
            cout << "Must specify more than 1 event." << endl;
            exit(-1);
         }
      } catch (const boost::bad_lexical_cast&) {
         cout << argv[1] << " is not an unsigned integer." << endl;
         exit(-1);
      }
   }

   if (argc > 2) {
      try {
         storage::n_per_mod = boost::lexical_cast<unsigned int>(argv[2]);
         storage::n_hits = storage::n_dom * storage::n_mod * storage::n_per_mod;
         if (storage::n_per_mod % 16 != 0) {
            cout << "number of hits per module must be a multiple of 16." << endl;
            exit(-1);
         }
      } catch (const boost::bad_lexical_cast&) {
         cout << argv[2] << " is not an unsigned integer." << endl;
         exit(-1);
      }
   }

   // bool do_fill = (argc > 2 && std::string{argv[2]} == "--fill");

   // The size is of the queue is determined by the number of
   // processors
   queue_t queue; queue.resize(n_processors);

   std::vector<zmq::pollitem_t> items; items.resize(n_generators + n_processors + 1);

   using starter_t = std::function<void(size_t)>;
   starter_t start_generator = [&queue] (size_t i) {
      generator(i, queue, seed0 + i, seed1 + i);
   };

   starter_t start_processor = [&queue] (size_t i) {
      processor(i, queue);
   };

   starter_t start_timer = timer;

   using workers_t = std::vector<std::tuple<std::thread, zmq::socket_t>>;
   workers_t generators; generators.reserve(n_generators);
   workers_t timers; timers.reserve(1);
   workers_t processors; processors.reserve(n_processors);

   // Prepare processing
   prepare_processing();

   // Start all workers
   size_t conn_id = 0;
   for (auto& entry : {make_tuple(std::ref(generators), start_generator, n_generators),
                       make_tuple(std::ref(processors), start_processor, n_processors),
                       make_tuple(std::ref(timers), start_timer, size_t{1})}) {
      auto& workers = std::get<0>(entry);
      auto start = std::get<1>(entry);
      auto n = std::get<2>(entry);

      for (size_t i = 0; i < n; ++i) {
         zmq::socket_t control = zmqSvc().socket(zmq::PAIR);
         zmq::setsockopt(control, zmq::LINGER, 0);
         auto con = connection(conn_id);
         control.bind(con.c_str());
         workers.emplace_back(std::thread{ [&start, conn_id] { start(conn_id); } }, std::move(control));
         items[conn_id] = {std::get<1>(workers.back()), 0, ZMQ_POLLIN, 0};
         ++conn_id;

         // I don't know why, but this prevents problems. Probably
         // some race condition I haven't noticed.
         std::this_thread::sleep_for(std::chrono::milliseconds{10});
      }
   }

   int n_events = 0;

   // keep track of which processors are ready
   std::bitset<n_generators> generators_ready;
   // Processors start ready
   std::bitset<n_processors> processors_ready;

   // Circular index which slice to process next
   size_t processor = 0;
   // Circular index which generator to poll first
   size_t generator = 0;

   // Wait for at least one event to be ready
   zmq::poll(&items[0], n_generators, -1);

   // Wait for all processors to be ready
   while (processors_ready.count() < processors_ready.size()) {
      boost::optional<int> n;
      do {
         try {
            n = zmq::poll(&items[n_generators], n_processors, -1);
         } catch (const zmq::error_t& err) {
            if (err.num() == EINTR) continue;
         }
      } while (!n);
      for (size_t i = 0; i < n_processors; ++i) {
         if (items[n_generators + i].revents & ZMQ_POLLIN) {
            auto& socket = std::get<1>(processors[i]);
            auto d = zmqSvc().receive<size_t>(socket);
            processors_ready[i] = true;
            cout << "processor " << std::setw(2) << i
                 << " device " << d << " ready." << endl;
         }
      }
   }

   // send go to timer
   zmqSvc().send(std::get<1>(timers.back()), true);

   while (n_events < max_events) {

      // Wait for timer signal
      boost::optional<int> n;
      do {
         try {
            n = zmq::poll(&items[0], items.size(), -1);
         } catch (const zmq::error_t& err) {
            if (err.num() == EINTR) continue;
         }
      } while (!n);

      // Check if any processors are ready
      for (size_t i = 0; i < n_processors; ++i) {
         if (items[n_generators + i].revents & ZMQ_POLLIN) {
            auto& socket = std::get<1>(processors[i]);
            auto r = zmqSvc().receive<bool>(socket);
            if (r) {
               processors_ready[i] = true;
            } else {
               cout << "processor " << i << " sent message, but not ready." << endl;
            }
         }
      }

      // Check if any generators are ready
      for (size_t i = 0; i < n_generators; ++i) {
         if (items[i].revents & ZMQ_POLLIN) {
            auto& socket = std::get<1>(generators[i]);
            auto r = zmqSvc().receive<bool>(socket);
            if (r) {
               generators_ready[i] = true;
            }
         }
      }

      if (items[n_generators + n_processors].revents & ZMQ_POLLIN) {
         zmqSvc().receive<bool>(std::get<1>(timers.back()));
         n_events += 1;

         // Timer fired, a slice should be sent for processing
         // Find a processor that is ready to process
         boost::optional<size_t> slice_index;
         if (!processors_ready.count()) {
            cout << "No processor ready to accept slice" << endl;
         } else {
            for (size_t i = 0; i < n_processors; ++i) {
               size_t idx = (i + processor) % n_processors;
               if (processors_ready[idx]) {
                  slice_index = idx;
                  break;
               }
            }
            processor = (processor == n_processors - 1) ? 0 : processor + 1;
         }

         // We have a spot where a slice can be put
         if (slice_index && !generators_ready.count()) {
            cout << "No generator ready to provide slice" << endl;
         } else if (slice_index) {
            // Find a generator that is ready and tell it where to put its slice
            size_t gen_idx = 0;
            for (size_t i = 0; i < n_generators; ++i) {
               gen_idx = (i + generator) % n_generators;
               if (generators_ready[gen_idx]) {
                  auto& socket = std::get<1>(generators[gen_idx]);
                  zmqSvc().send(socket, *slice_index);
                  zmqSvc().receive<bool>(socket);
                  generators_ready[gen_idx] = false;
                  break;
               }
            }
            generator = (generator == n_generators - 1) ? 0 : generator + 1;

            // send message to processor to process the slice
            // the slice_index also uniquely identifies the processor.
            auto& socket = std::get<1>(processors[*slice_index]);
            zmqSvc().send(socket, *slice_index);
            processors_ready[*slice_index] = false;

            cout << "slice " << std::setw(6) << n_events
                 << " gen " << std::setw(2) << gen_idx
                 << " proc " << std::setw(2) << *slice_index << endl;
         }
      }
   }

   // Send stop signal to all threads
   for (auto& workers : {std::ref(generators), std::ref(processors)}) {
      for (auto& worker : workers.get()) {
         zmqSvc().send(std::get<1>(worker), int{-1});
      }
   }
   zmqSvc().send(std::get<1>(timers.back()), true);

   // Join threads
   for (auto& workers : {std::ref(generators),
            std::ref(processors),
            std::ref(timers)}) {
      for (auto& worker : workers.get()) {
         std::get<0>(worker).join();
      }
   }

   // if (do_fill) {
   //    auto times = std::get<0>(queue[0]);
   //    auto values = std::get<1>(queue[0]);

   //    cout << "filling" << endl;

   //    TFile output_file{"../data/differences_vect.root", "recreate"};
   //    TTree tree{"diff_tree", "diff_tree"};
   //    int diff = 0.;
   //    int val = 0;
   //    tree.Branch("diff", &diff, "diff/I");
   //    tree.Branch("val", &val, "val/I");

   //    for (size_t i = 0; i < times.size() - 1; ++i) {
   //       if (i % 5000000ul == 0) {
   //          cout << "filling hit: " << setw(10) << i << endl;
   //       }
   //       diff = times[i + 1] - times[i];
   //       val = values[i + 1];
   //       tree.Fill();
   //    }

   //    output_file.WriteTObject(&tree);
   //    output_file.Close();

   //    cout << times.size() << endl;
   // }

   // for (int i = 0; i < 4; ++i) {
   //    for (int j = 0; j < float_v::Size; ++j) {
   //       cout << setw(10) << std::right << times[float_v::Size * i + j] << endl;
   //    }
   // }
}
