#include <vector>

#include <TRandom.h>
#include <TMath.h>

int main() {

   gRandom->SetSeed(34136856);
   std::vector<float> numbers(45000000);

   for (int i = 0; i < numbers.size(); ++i) {
      numbers[i] = gRandom->Exp(-1.5);
   }

}
