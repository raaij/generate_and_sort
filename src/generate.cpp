#include <iostream>

#include <boost/optional.hpp>

#include <Vc/Vc>

#include <instrset.h>
#include <vectori256.h>
#include <ranvec1.h>

#include "generate.h"
#include "ZeroMQSvc.h"
#include "functions.h"
#include "storage.h"

namespace {

   using std::cout;
   using std::endl;

   using Vc::float_v;
   using Vc::int_v;

   // 2 * pi
   const float two_pi = 2.0 * 3.14159265358979323846;

   // From ROOT fit of exp(a + b x) to exponential part:
   const float a = 1.14618e+01, b = -2.10460e-4;
   // From ROOT fit to flat part of histogram:
   const float c = 85466.6;
   const float edge = -250.;
   // the point where we go from flat to exponential
   const float tp = (log(c) - a) / b;
   const float flat_width = tp - edge;

   // normalization constant
   const float norm = (tp - edge) * c - 1 / b * exp(a + b * tp);
   const float frac_flat = (tp - edge) * c / norm;
   const float frac_exp = 1 - frac_flat;

   const float min_r = exp(tp * b);

   // from fit to Gaussian part of ToT distribution
   const float_t tot_mean = 26.936;
   const float_t tot_sigma = 2.44078;

   const int random_method = 2;
}

inline Vc::int_v scan_AVX(Vc::int_v x) {
   // first shift then add
   auto t0 = permute8i<3, 0, 1, 2, 7, 4, 5, 6>(x.data());
   auto t1 = permute8i<-1, -1, -1, -1, 0, 1, 2, 3>(t0);
   x += _mm256_blend_epi32(t0, t1, 0x11);

   // second shift then add
   t0 = permute8i<2, 3, 0, 1, 6, 7, 4, 5>(x.data());
   t1 = permute8i<-1, -1, -1, -1, 0, 1, 2, 3>(t0);
   x += _mm256_blend_epi32(t0, t1, 0x33);

   // final shift and add
   x += int_v{permute8i<-1, -1, -1, -1, 0, 1, 2, 3>(x.data())};
   return x;
}

std::tuple<storage_t, storage_t> generate(Ranvec1& random) {

   // Assume times.size() is multiple of 8 here
   assert(storage::n_hits % 16 == 0);

   storage_t times; times.resize(storage::n_hits);
   storage_t values; values.resize(storage::n_hits);

   // First generate some data
   for (int dom = 0; dom < storage::n_dom; ++dom) {
      for (int mod = 0; mod < storage::n_mod; ++mod) {
         int_v offset;
         for (int hit = 0; hit < storage::n_per_mod; hit += float_v::size()) {
            // Generate times
            auto tmp = random.random8f();
            float_v r{tmp};

            // We have to rescale our drawn random numbers here to make sure
            // we have the right coverage
            r(r > frac_flat) = 1.f / b * log(min_r - (r - frac_flat) / frac_exp * min_r);
            r(r < frac_flat) = edge + flat_width * (r * 1.f / frac_flat);

            auto tmp_i = simd_cast<int_v>(r);

            // Prefix sum from stackoverflow:
            // https://stackoverflow.com/questions/19494114/parallel-prefix-cumulative-sum-with-sse
            auto out = scan_AVX(tmp_i);
            out += offset;

            int idx = storage::n_mod * storage::n_per_mod * dom + storage::n_per_mod * mod + hit;

            out.store(&times[idx]);

            //broadcast last element
            offset.data() = permute8i<7, 7, 7, 7, 7, 7, 7, 7>(out.data());

            // Generate ToT as a gauss and pmt flat only do it every
            // other pass to make use of the double output of the
            // Box-Muller method
            // When filling, fill the past and current indices
            if ((hit / 8) % 2 == 1) {
               int_v pmt1{random.random8i(0, 31)};
               int_v pmt2{random.random8i(0, 31)};

               auto u1 = float_v{random.random8f()};
               auto u2 = float_v{random.random8f()} * two_pi;

               auto fact = sqrt(-2.0f * log(u1));
               float_v z0, z1;
               sincos(u2, &z0, &z1);

               z0 = fact * tot_sigma * z0 + tot_mean;
               z1 = fact * tot_sigma * z1 + tot_mean;

               auto val0 = simd_cast<int_v>(z0) | (pmt1 << 8) | ((100 * (dom + 1) + mod + 1) << 13);
               val0.store(&values[idx - int_v::size()]);

               auto val1 = simd_cast<int_v>(z1) | (pmt2 << 8) | ((100 * (dom + 1) + mod + 1) << 13);
               val1.store(&values[idx]);
            }
         }
      }
   }
   return {std::move(times), std::move(values)};
}

void generator(const size_t gid, queue_t& queue, int seed0, const int seed1) {

   Ranvec1 random{random_method};
   random.init(seed0, seed1);

   zmq::socket_t control = zmqSvc().socket(zmq::PAIR);
   zmq::setsockopt(control, zmq::LINGER, 0);

   auto con = connection(gid);
   try {
      control.connect(con.c_str());
   } catch (const zmq::error_t& e) {
      cout << "failed to connect connection " << con << endl;
      throw e;
   }

   zmq::pollitem_t items [] = {
      {control, 0, ZMQ_POLLIN, 0},
   };

   storage_t times, values;

   while (true) {

      // Generate a slice
      std::tie(times, values) = generate(random);

      // Indicate to the main thread that a slice is ready
      zmqSvc().send(control, true);

      // Wait until it is needed
      zmq::poll(&items[0], 1, -1);

      boost::optional<int> idx;
      if (items[0].revents & ZMQ_POLLIN) {
         idx = zmqSvc().receive<int>(control);
         if (*idx == -1) break;
      }

      if (idx) {
         std::swap(std::get<0>(queue[*idx]), times);
         std::swap(std::get<1>(queue[*idx]), values);
         zmqSvc().send(control, true);
      }
   }
}
