#include <instrset.h>
#include <vectori256.h>
#include <vectorf256.h>

#include <Vc/Vc>
#include <Vc/Allocator>
#include <Vc/Memory>
#include <Vc/vector>

#include <iostream>
#include <iomanip>
#include <vector>
#include <limits>

namespace {
   using Vc::float_v;
   using Vc::int_v;
   using std::cout;
   using std::endl;
   using std::vector;
   using std::setw;

   // using int_v = Vc::SimdArray<int, float_v::size()>;
}

inline __m256 scan_AVX(__m256 x) {
    __m256 t0, t1;
    //shift1_AVX + add
    t0 = _mm256_permute_ps(x, _MM_SHUFFLE(2, 1, 0, 3));
    cout << float_v{t0} << endl;
    t1 = _mm256_permute2f128_ps(t0, t0, 41);
    cout << float_v{t1} << endl;
    x = _mm256_add_ps(x, _mm256_blend_ps(t0, t1, 0x11));
    cout << float_v{x} << endl;

    //shift2_AVX + add
    t0 = _mm256_permute_ps(x, _MM_SHUFFLE(1, 0, 3, 2));
    t1 = _mm256_permute2f128_ps(t0, t0, 41);
    x = _mm256_add_ps(x, _mm256_blend_ps(t0, t1, 0x33));
    //shift3_AVX + add
    x = _mm256_add_ps(x,_mm256_permute2f128_ps(x, x, 41));
    return x;
}

int main() {

   cout << std::numeric_limits<int>::max() << endl;

   int n[8] = {1, 2, 3, 4, 5, 6, 7, 8};
   int_v test; test.load(n);
   cout << test << endl;

   float m[8] = {1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f};
   float_v test_m; test_m.load(m);
   cout << float_v{scan_AVX(test_m.data())} << endl;

   cout << endl;
   auto t0 = permute8i<3, 0, 1, 2, 7, 4, 5, 6>(test.data());
   auto t1 = permute8i<-1, -1, -1, -1, 0, 1, 2, 3>(t0);
   test += _mm256_blend_epi32(t0, t1, 0x11);

   t0 = permute8i<2, 3, 0, 1, 6, 7, 4, 5>(test.data());
   t1 = permute8i<-1, -1, -1, -1, 0, 1, 2, 3>(t0);
   test += _mm256_blend_epi32(t0, t1, 0x33);

   test += int_v{permute8i<-1, -1, -1, -1, 0, 1, 2, 3>(test.data())};
   cout << test << endl;
}
   // t0 = _mm256_permute_ps(x, _MM_SHUFFLE(2, 1, 0, 3));
   // t1 = _mm256_permute2f128_ps(t0, t0, 41);
   // x = _mm256_add_ps(x, _mm256_blend_ps(t0, t1, 0x11));
