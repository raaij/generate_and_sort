#include "process.h"

int processing::n_device = 1;


namespace {
   using std::cout;
   using std::endl;
}

void prepare_processing() {
}

size_t setup(const size_t) {
   return 0;
}

void process(const size_t, storage_t& times, storage_t& values) {
}

void cleanup(const size_t) {

}
